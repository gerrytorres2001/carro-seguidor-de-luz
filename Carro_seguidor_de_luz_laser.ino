/*Carro seguidor de luz laser
 equipo Siacel
 Teclogocio de veracruz */
int valorA2; //variable sensor luz izquierdo, controla motor derecho
 int valorA1; //variable sensor luz derecho, controla motor izquierdo
void setup () {
 pinMode (13, OUTPUT); //Salida digital control motor izquierdo
 pinMode (11, OUTPUT); //Salida digital control motor derecho

 Serial.begin (9600); // Comunicación ARDUINO ordenador
}
void loop() {
 valorA2=analogRead(A2); //Valor señal de entrada sensor izquierdo
 valorA1=analogRead(A1); // Valor señal de entrada sensor derecho
 Serial.println (valorA1); //Visualizar valor señal en ordenador
 Serial.println (valorA2); // Visualizar valor señal en ordenador

 if (valorA2>=700) {
 digitalWrite (11, HIGH); //Activa motor derecho
 }
 else {
 digitalWrite (11, LOW); //Desactiva motor derecho
 }

 if (valorA1>=700) {
 digitalWrite (13, HIGH); //Activa motor izquierdo
 }
 else {
 digitalWrite (13, LOW); // Desactiva motor izquierdo
 }
}

 
